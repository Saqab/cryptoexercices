// BELLITOU Halima
// MUKHTAR Saqab

/* XOR s�par� 2 par 2
XOR0: 01 45 41 04 53 50 18 09 0b 4e 06 00 0b 59 41 50 16 17 10 1d 0b 54 54 02 49 07 01 1c 1c 1a 00 
XOR1: 1d 48 04 4d 43 05 1e 13 0b 00 1d 4e 17 4c 00 4e 53 0c 10 52 11 1b 50 4d 1a 16 10 1b 0a 1a 00
XOR2: 1e 48 04 03 00 03 04 0e 1b 02 0d 4e 10 45 41 4d 16 00 17 52 11 1b 00 09 06 53 07 01 06 1d 11
XOR3: 00 00 15 05 49 1e 07 41 1a 06 0c 17 47 53 09 4f 06 09 07 52 03 1b 4c 01 06 04 53 01 06 03 00
XOR4: 1d 48 08 1e 00 19 1f 41 1e 1b 1b 0b 15 00 15 48 12 0b 43 06 0d 15 54 4d 06 1d 16 49 06 1d 00
XOR5: 07 4f 15 4d 4f 1e 09 41 0d 0f 0d 0b 13 00 08 53 53 07 06 06 11 11 52 4d 1d 1b 12 07 4f 27 00

MSG0: BB 3A 65 F6 F0 03 4F A9 57 F6 A7 67 69 9C E7 FA BA 85 5A FB 4F 2B 52 0A EA D6 12 94 4A 80 1E
MSG1: BA 7F 24 F2 A3 53 57 A0 5C B8 A1 67 62 C5 A6 AA AC 92 4A E6 44 7F 06 08 A3 D1 13 88 56 9A 1E
MSG2: A6 72 61 BB B3 06 51 BA 5C F6 BA 29 7E D0 E7 B4 E9 89 4A A9 5E 30 02 47 F0 C0 02 8F 40 9A 1E
MSG3: A5 72 61 F5 F0 00 4B A7 4C F4 AA 29 79 D9 A6 B7 AC 85 4D A9 5E 30 52 03 EC 85 15 95 4C 9D 0F
MSG4: BB 3A 70 F3 B9 1D 48 E8 4D F0 AB 70 2E CF EE B5 BC 8C 5D A9 4C 30 1E 0B EC D2 41 95 4C 83 1E
MSG5: A6 72 6D E8 F0 1A 50 E8 49 ED BC 6C 7C 9C F2 B2 A8 8E 19 FD 42 3E 06 47 EC CB 04 DD 4C 9D 1E
MSG6: BC 75 70 BB BF 1D 46 E8 5A F9 AA 6C 7A 9C EF A9 E9 82 5C FD 5E 3A 00 47 F7 CD 00 93 05 A7 1E
 	   0  1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30
 */
package org.dador;

/**
 *
 */
public class MultiTimePad {


    /**
     * Main function. Loads cryptogram and displays decryption
     * @param args
     */
    public static void main(final String[] args) {
        String msg0 = "BB3A65F6F0034FA957F6A767699CE7FABA855AFB4F2B520AEAD612944A801E";
        String msg1 = "BA7F24F2A35357A05CB8A16762C5A6AAAC924AE6447F0608A3D11388569A1E";
        String msg2 = "A67261BBB30651BA5CF6BA297ED0E7B4E9894AA95E300247F0C0028F409A1E";
        String msg3 = "A57261F5F0004BA74CF4AA2979D9A6B7AC854DA95E305203EC8515954C9D0F";
        String msg4 = "BB3A70F3B91D48E84DF0AB702ECFEEB5BC8C5DA94C301E0BECD241954C831E";
        String msg5 = "A6726DE8F01A50E849EDBC6C7C9CF2B2A88E19FD423E0647ECCB04DD4C9D1E";
        String msg6 = "BC7570BBBF1D46E85AF9AA6C7A9CEFA9E9825CFD5E3A0047F7CD009305A71E";
        String[] messages = new String[]{msg0, msg1, msg2, msg3, msg4, msg5, msg6};
        byte[] key;
        byte[][] byteArrayMsg;
        int nbMsg = messages.length;
        byte[] tmpByteMsg;
        int i;
        byteArrayMsg = new byte[nbMsg][];

        System.out.println("Original Cryptograms :");
        for (i = 0; i < nbMsg; i++) {
            System.out.println(messages[i]);
            tmpByteMsg = HexConverters.toByteArrayFromHex(messages[i]);
            byteArrayMsg[i] = tmpByteMsg ;
        }
        
        

        System.out.println();

        key = new byte[msg1.length() / 2];
        
        key[0] = (byte)0x49^(byte)0xBB;
        key[1]= (byte)0x1a;
        key[2] = 0x04;
        key[3] = (byte)0x9B;
        key[4] = (byte)0xD0;
        key[5] = 0x73;
        key[6] = (byte)0x48^(byte)0x6B;
        key[7] = (byte)0xC8;
        key[8] = (byte)0x6E^(byte)0x57;
        key[9] = (byte)0x6E^(byte)0xF6;
        key[10] = (byte)0x69^(byte)0xA7;
        key[11] = (byte)0x6E^(byte)0x67;
        key[12] = 0x0E;
        key[13] = (byte)0x20^(byte)0x9C;
        key[14] = (byte)0x86;
        key[15] = (byte)0xda;
        key[16] = (byte)0x73^(byte)0xBA;
        key[17] = (byte)0x62^(byte)0x82;
        key[18] = (byte)0x63^(byte)0x5A;
        key[19] = (byte)0x89;
        key[20] = (byte)0x74^(byte)0x5E;
        key[21] = 0x5F;
        key[22] = 0x72;
        key[23] = 0x67;
        key[24] =(byte) 0x83;
        key[25] = (byte)0xA5;
        key[26] = 0x61;
        key[27] = (byte)0xFD;
        key[28] = 0x25;
        key[29] = (byte)0x6E^(byte)0x80;
        key[30] = (byte)0x2E^(byte)0x1E;
        
        /*for(int j=0; j<messages.length-1;j++)
        	System.out.println("XOR"+j+": "+HexConverters.
        			toHexFromByteArray(HexConverters.xorArray(HexConverters.toByteArrayFromHex(msg0),
        					HexConverters.toByteArrayFromHex(messages[j+1]))));
        
        
        
    	System.out.println("XOR A-Z : "+HexConverters.
    			toHexFromByteArray(HexConverters.xorArray(HexConverters.toByteArrayFromHex("4"),
    					HexConverters.toByteArrayFromHex("5"))));
    	*/
    	

        System.out.println();
        System.out.println("Decoded messages :");
        for (i = 0; i < nbMsg; i++) {
            tmpByteMsg = HexConverters.xorArray(key, byteArrayMsg[i]);
            System.out.println(HexConverters.toPrintableString(tmpByteMsg));
        }
    }

}